import React, { Suspense, lazy, useEffect, useState } from 'react';
import logo from './logo.svg';
import './App.css';

// import Counter from 'remote/Counter'

const Counter = lazy(() => import('remote/Counter'))

function App() {
  const [count, setCount] = useState(0)
  return (
    <div className="App">
      <h1>This is Host App</h1>
      <Suspense fallback={<div>Loading ...</div>}>
        <Counter count={count} onClick={() => setCount(count + 1)} />
      </Suspense>  
    </div>
  );
}

export default App;
